// PS Configuration Library simplifies access to, and the unmarshalling of configuration
// values from Vault and Consul.
package configuration

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	vaultapi "github.com/hashicorp/vault/api"
	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
)

type IConfig interface {
	GetConfig() *Config
	Initialize(
		environment string,
		token string,
	) error
	GetSecrets(
		path string,
	) (map[string]interface{}, error)
	SetSecrets(
		secrets map[string]interface{},
	) error
	GetConsulValues(
		path string,
		key string,
	) ([]byte, error)
	LoadConfig() (map[string]interface{}, error)
}

type Config struct {
	IConfig
}

// GetConfig returns an implementation of the IConfig interface.
func GetConfig() *Config {
	var configuration = Config{}
	return &configuration
}

// Sub Struct for the storage of an environment file.
type Environment struct {
	Consul Details `json:"consul"`
	Vault  Details `json:"vault"`
}

// Struct for the storage of an environment file.
type Details struct {
	Nodes []string `json:"nodes"`
	Paths []string `json:"paths"`
}

// Struct for the storage of results from the initial Vault API calls.
type Seal_status struct {
	Sealed   bool `json:"sealed"`
	T        bool `json:"t"`
	N        bool `json:"n"`
	Progress bool `json:"progress"`
}

var remote_config Environment
var vault_client *vaultapi.Logical
var runtime_viper = viper.New()
var reset bool = false

// InitializeVault loads URL's from the environtment files specified, locates an
// unsealed Vault node, and sets up a connection for usage. It also loads the URL's
// needed to connect to Consul from the environment file.
func (config *Config) Initialize(
	environment string,
	token string,
) error {
	log.Println("Configuration library reading environment files...")
	viper.Reset()
	viper.SetConfigName(environment)
	viper.AddConfigPath(".")
	viper.AddConfigPath("../")
	viper.AddConfigPath("../..")
	viper.SetConfigType("yaml")

	err := viper.ReadInConfig()
	if err != nil {
		return err
	}

	viper.Unmarshal(&remote_config)

	log.Printf("Using environment config: %#+v\n", remote_config)

	log.Println("Contacting Vault nodes to find unsealed instance...")
	client := &http.Client{}

	var vault_status Seal_status
	var vault_addr string = "unset"

	for _, url := range remote_config.Vault.Nodes {
		req, err := http.NewRequest("GET", url, nil)

		req.Header.Add("X-Vault-Token", token)

		log.Printf("Trying vault instance at %s\n", url)
		res, err := client.Do(req)
		if err != nil {
			log.Printf("Error connecting to Vault node: %s\n", err)
			continue
		}

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			log.Printf("Error connecting to Vault node: %s\n", err)
			continue
		}

		json.Unmarshal(body, &vault_status)

		if !vault_status.Sealed {
			vault_addr = url
			log.Printf("Found unsealed vault instance at %s\n", vault_addr)
			break
		}
	}

	if vault_addr == "unset" {
		return errors.New("unable to find an unsealed instance of Vault")
	}

	log.Printf("setting vault address to %s\n", vault_addr)
	os.Setenv("VAULT_ADDR", vault_addr)
	log.Printf("setting vault token to %s\n", token)
	os.Setenv("VAULT_TOKEN", token)

	vclient, err := vaultapi.NewClient(vaultapi.DefaultConfig())
	if err != nil {
		return err
	}

	vclient.SetToken(token)
	vault_client = vclient.Logical()

	return nil
}

// GetSecrets loads data from the Vault path provided.
func (config *Config) GetSecrets(
	path string,
) (map[string]interface{}, error) {
	values := make(map[string]interface{})

	result, err := vault_client.Read(path)
	if err != nil {
		return values, err
	}

	if result != nil {
		values = result.Data
	}

	return values, err
}

// SetSecrets stores the data provided in the Vault path provided. This is useful
// for storing any sensitive runtime data.
func (config *Config) SetSecrets(
	path string,
	secrets map[string]interface{},
) error {
	_, err := vault_client.Write(path, secrets)
	return err
}

// LoadConfig retrieves YAML configurations first from Consul nodes at the URL's
// provided in the environment file specified, and/or from the config file specified
// when unable to load from Consul.
func (config *Config) GetConsulValues(
	path string,
) (map[string]interface{}, error) {
	var remote_success bool = false
	var values interface{}
	var retval map[string]interface{}
	var err error

	runtime_viper = viper.New()
	runtime_viper.AddConfigPath(".")
	runtime_viper.AddConfigPath("../")
	runtime_viper.AddConfigPath("../..")
	runtime_viper.SetConfigType("yaml")

	for _, url := range remote_config.Consul.Nodes {
		var myvalues interface{}
		runtime_viper.AddRemoteProvider("consul", url, path)
		err = runtime_viper.ReadRemoteConfig()
		if err != nil {
			log.Printf("Unable to read config from remote provider at %s because: %+v\n", url, err)
		} else {
			log.Printf("Read remote configuration from %s\n", url)
			runtime_viper.Unmarshal(&myvalues)
			values = myvalues
			remote_success = true
			break
		}
	}

	if !remote_success {
		split_path := strings.Split(path, "/")
		key := split_path[len(split_path)-1]
		runtime_viper.SetConfigFile(key)
		err = runtime_viper.ReadInConfig()
		if err != nil {
			log.Printf("Unable to read config from file %s because: %s\n", key, err.Error())
		} else {
			runtime_viper.Unmarshal(&values)
		}
	}

	retval = config.ConvertValues(values)
	return retval, err
}

// LoadConfig merges every remote path in your env file into one map and returns it.
func (config *Config) LoadConfig() map[string]interface{} {
	conf := make(map[string]interface{})

	for _, path := range remote_config.Vault.Paths {
		split_path := strings.Split(path, "/")
		key := split_path[len(split_path)-1] + "_secrets"
		log.Printf("Getting secrets from path: %s\n", path)
		secrets, err := config.GetSecrets(path)
		if err != nil {
			log.Fatal(err)
		}
		conf[key] = secrets
	}

	for _, path := range remote_config.Consul.Paths {
		var conf_key string
		var err error
		split_path := strings.Split(path, "/")
		file := split_path[len(split_path)-1]
		if strings.Contains(file, ".") {
			split_file := strings.Split(file, ".")
			conf_key = split_file[0]
		} else {
			conf_key = file
		}
		log.Print(path)
		conf[conf_key], err = config.GetConsulValues(path)
		if err != nil {
			log.Print(err)
		}
	}

	conf = config.ConvertValues(conf)

	return conf
}

// ConvertValues takes an interface, and returns map[string]interface{}. This
// makes it possible to use json encoding/decoding to access configuration data
// returned from Consul.
func (config *Config) ConvertValues(
	values interface{},
) map[string]interface{} {
	conversion := values.(map[string]interface{})

	result := make(map[string]interface{})

	for key, val := range conversion {
		switch val := val.(type) {
		case map[interface{}]interface{}:
			result[key] = config.MakeKeyString(val)
		default:
			result[key] = val
		}
	}

	return result
}

// MakeKeyString is a recursive function that will convert any nested values in
// a map from map[interface{}]interface{} type to map[string]interface{}.
func (config *Config) MakeKeyString(
	values map[interface{}]interface{},
) map[string]interface{} {

	result := make(map[string]interface{})

	for key, val := range values {
		switch key := key.(type) {
		case string:
			switch val := val.(type) {
			case map[interface{}]interface{}:
				result[key] = config.MakeKeyString(val)
			default:
				result[key] = val
			}
		}
	}

	return result
}
