PS Configuration Library
------------------------

The config library provides access to common configuration data structures and the
logic to load external configuration used throughout the Powerstandings platform.

##Installation

Right now the ISDC Git repository runs on port :7999. This breaks vendoring for
cross platform languages. So, for now we have to work around this by using a 3rd
party Git hosting provider, with security and access control. I have chosen 
BitBucket because it provides all of these things for free, it is basically just
Atlassian's hosted Stash platform, and because I already had an account.

For the time being, in order to work on, or install PS libraries, you will need
your own Bitbucket account. Be sure to add your public key to your BitBucket account.
Once you have an account send Camron (clevanger@insidesales.com) your Bitbucket 
username and email address so that you can be given access to the repositories.

Alternatively, if you do not need write access, or if the access is for a build
system/other automation, just send Camron the public key and access can be granted
that way.

Once you have an account, and have sent your info to Camron, you need to configure
Git to use the proper URL's for BitBucket, otherwise you will constantly be prompted
for credentials.

Add the following lines to `~/.gitconfig`:

```
[url "git@bitbucket.org:"]
     insteadOf = https://bitbucket.org/
```

Once you have access:

`go get bitbucket.org/noisewaterphd/ps-configuration-library`

Then to save the version of dependency to your project:

`godep save`

##Usage

```
import "bitbucket.org/noisewaterphd/ps-configuration-library"
import "encoding/json"

// Get an instance of the config library
config := configuration.GetConfig()

// You must initialize the library. Pass in the environment file, and your Vault token.
config.Initialize("prod", "some-token-string")

// To load usernames, passwords, or other sensitive data we use GetSecrets().
// Pass in the path where vault keeps your secrets, and the function returns you a key/value map.
secrets, err := config.GetSecrets("some/path/string")
if err != nil {
	log.Fatal("Unable to load secret data")
}

username := secrets["username"]

// To load general configuration from Consul, or config file:

// Use LoadConfig(path_in_consul, key_or_file), it returns a byte array which you can unmarshal
// using the encoding libraries. You can either build a struct to unmarshal the config to, or
// you can unmarshal it to a simple map[string]interface{}

// The following set of structs could handle the following YAML being
// unmarshalled into it:

// ---
// answer:
//   everything: 42
//   life: 42
//   love_windows: false
//   universe: 42
// database:
//   connections:
//     - "somewhere.com:3300"
//     - "somewhereelse.com:3300"


type SubConfigStruct struct {
	Everything     int  `json:"everything"`
	Life           int  `json:"life"`
	Universe       int  `json:"universe"`
	Love_windows   bool `json:"love_windows"`
}

type AnotherSubConfigStruct struct {
	Connections []string `json:"connections"`
}

type MyConfig struct {
	Stuff     SubConfigStruct        `json:"answer"`
	Database  AnotherSubConfigStruct `json:"database"`
}

myconfig := MyConfig{}

results, err := config.LoadConfig("/isdc/dev/ps/configuration/v1/test/", "test_config.yml")
if err != nil {
	log.Fatal(err)
}

err = json.Unmarshal(results, &myconfig)
if err != nil {
	log.Fatal(err)
}

the_answer_to_life := myconfig.Stuff.Life
```

##Notes

If you name your Consul key the same name as the config file you upload,
the config library will fall back on that file in the event it cannot connect
to a Consul node.

So if the Consul path `/isdc/prod/ps/throwdowns/v1/config.yml` contains you config values,
then in the event Consul cannot be reached, `./config.yml` or `../config.yml` will be parsed.
