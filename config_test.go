package configuration

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"

	"github.com/camronlevanger/mapstructure"
)

func TestNoEnvironmentFile(t *testing.T) {

	config := GetConfig()

	err := config.Initialize("non_existing_file", os.Getenv("VAULT_TEST_TOKEN"))
	if err == nil {
		t.Error("Should return an error when environment file isn't present.")
	}
}

func TestBadEnvironmentFile(t *testing.T) {

	config := GetConfig()

	err := config.Initialize("real_bad", os.Getenv("VAULT_TEST_TOKEN"))
	if err == nil {
		t.Error("Should return an error when a bad environment file is given.")
	}
}

func TestVault(t *testing.T) {

	secrets_path := "secret/isdc/dev/ps/v1/test_secrets"

	config := GetConfig()

	err := config.Initialize("test_env", os.Getenv("VAULT_TEST_TOKEN"))
	if err != nil {
		t.Error("Should properly initialize config library: ", err)
	}

	err = config.SetSecrets(
		secrets_path,
		map[string]interface{}{
			"doctor":  "who",
			"jessica": "jones",
		})
	if err != nil {
		t.Error("Should be able to set Vault secrets: ", err)
	}

	secrets, err := config.GetSecrets(secrets_path)
	if err != nil {
		t.Error("Should be able to read Vault secrets: ", err)
	}

	if secrets["doctor"] != "who" {
		t.Error("Should get proper configuration. Expected 'who', but got: ", secrets["doctor"])
	}

	if secrets["jessica"] != "jones" {
		t.Error("Should get proper configuration. Expected 'jones', but got: ", secrets["doctor"])
	}

}

func TestConsul(t *testing.T) {

	type AnswerDetails struct {
		Everything int  `json:"everything"`
		Life       int  `json:"life"`
		Universe   int  `json:"universe"`
		Love       bool `json:"love_windows"`
	}

	type DatabaseDetails struct {
		Connections []string `json:"connections"`
	}

	type TestConfig struct {
		Answer   AnswerDetails   `json:"answer"`
		Database DatabaseDetails `json:"database"`
	}

	var read TestConfig

	config := GetConfig()

	err := config.Initialize("test_env", os.Getenv("VAULT_TEST_TOKEN"))
	if err != nil {
		t.Error("Should properly initialize config library: ", err)
	}

	results, err := config.GetConsulValues("/isdc/dev/ps/configuration/v1/test/test_config.yml")
	if err != nil {
		t.Error("Should be able to retrieve configuration from Consul: ", err)
	}

	jstring, err := json.Marshal(results)
	err = json.Unmarshal(jstring, &read)
	if err != nil {
		t.Error("Should be able to unmarshal config return: ", err)
	}

	if read.Answer.Life != 42 {
		t.Error("Should be able to read values returned. expected 42, but got: ", read.Answer.Life)
	}

	if read.Answer.Love {
		t.Error("I do not love Windows: ", read.Answer.Love)
	}

}

func TestLoadConfig(t *testing.T) {

	type AnswerDetails struct {
		Everything int  `mapstructure:"everything"`
		Life       int  `mapstructure:"life"`
		Universe   int  `mapstructure:"universe"`
		Love       bool `mapstructure:"love_windows"`
	}

	type Secrets struct {
		Doctor  string `mapstructure:"doctor"`
		Jessica string `mapstructure:"jessica"`
	}

	type DatabaseDetails struct {
		Connections []string `mapstructure:"connections"`
	}

	type Config struct {
		Database DatabaseDetails `mapstructure:"database"`
		Answer   AnswerDetails   `mapstructure:"answer"`
	}

	type TestConfig struct {
		Config               Config  `mapstructure:"config"`
		Test_secrets_secrets Secrets `mapstructure:"test_secrets_secrets"`
	}

	var conf TestConfig

	config := GetConfig()

	err := config.Initialize("test_env", os.Getenv("VAULT_TEST_TOKEN"))
	if err != nil {
		t.Error("Should properly initialize config library: ", err)
	}

	err = mapstructure.Decode(config.LoadConfig(), &conf)

	if err != nil {
		t.Error("Should be able to retrieve configuration from Consul: ", err)
	}

	fmt.Printf("%+v\n", conf)

	if conf.Config.Answer.Life != 42 {
		t.Error("Should be able to read values returned. expected 42, but got: ", conf.Config.Answer.Life)
	}

	if conf.Test_secrets_secrets.Doctor != "who" {
		t.Error("The Doctor is Who: ", conf.Test_secrets_secrets.Doctor)
	}

}
